<?php
require_once("../db.php");
require_once("../error_handler.php");

class CommentController
{
    public static function load($id)
    {
        $conn = Connection::get();
        $sql = $conn->prepare("SELECT * FROM comments WHERE id = ?");
        $sql->bind_param("i", $id);
        $sql->execute();
        $comment = $sql->get_result()->fetch_assoc();
        return $comment;
    }

    public static function create()
    {
        $sound_id = $_POST["sound_id"];
        $name = $_POST["name"];
        $content = $_POST["content"];
        $time = $_POST["time"];

        $conn = Connection::get();
        $sql = $conn->prepare("INSERT
            INTO comments (`sound_id`, `name`, `content`, `time`)
            VALUES (?, ?, ?, ?)");
        $sql->bind_param("issd", $sound_id, $name, $content, $time);
        $sql->execute();
        return $sql->insert_id;
    }
}
