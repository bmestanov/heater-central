<?php
class Connection
{
    private static $conn = null;

    public static function get()
    {
        if (isset(Connection::$conn)) {
            return Connection::$conn;
        }

        $config = json_decode(file_get_contents(__DIR__ . "/config.json"), true);
        Connection::$conn = new mysqli(
            $config["host"],
            $config["user"],
            $config["password"],
            $config["database"]
        );

        if (Connection::$conn->connect_error) {
            die("Connection failed: " . Connection::$conn->connect_error);
        }

        return Connection::$conn;
    }
}
