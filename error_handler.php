<?php
set_exception_handler(function () {
    header("Location: /500.php");
});

set_error_handler(function($errno ,$errstr) {
    header("Location: /500.php");
});