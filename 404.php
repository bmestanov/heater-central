<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>404</title>
    <link rel="stylesheet" href="/assets/css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <div class="center">
        <img src="/assets/img/404.gif" />
        <a href="/sounds">Go home</a>
    </div>
</body>

</html>