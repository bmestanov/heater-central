<?php
// Routing
require_once("./controller.php");
if (isset($_GET["id"])) {
    $result = SoundController::load($_GET["id"]);
    if (!$result) {
        require_once("../404.php");
    } else {
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            require_once("./show/index.php");
        } else {
            require_once("./delete.php");
        }
    }
    return;
}

require_once("./new/index.php");
