<?php
require_once("../db.php");
require_once("../error_handler.php");

class SoundController
{
    public static function load($id)
    {
        $conn = Connection::get();
        $sql = $conn->prepare("SELECT
            sounds.id as 'sound.id',
            sounds.uuid as 'sound.uuid',
            sounds.title as 'sound.title',
            sounds.uploader as 'sound.uploader',
            sounds.password as 'sound.password',
            sounds.file_path as 'sound.file_path',
            sounds.created_at as 'sound.created_at',
            comments.id as 'comment.id',
            comments.name as 'comment.name',
            comments.content as 'comment.content',
            comments.sound_id as 'comment.sound_id',
            comments.time as 'comment.time',
            comments.created_at as 'comment.created_at'
            FROM sounds
            LEFT JOIN comments ON comments.sound_id = sounds.id
            WHERE sounds.uuid = ?
            ORDER BY comments.time ASC
        ");
        $sql->bind_param("s", $_GET["id"]);
        $sql->execute();
        $result = $sql->get_result();
        $sound = ["comments" => []];

        if ($result->num_rows == 0) {
            return null;
        } else {
            $record = $result->fetch_assoc();
            $sound["id"] = $record["sound.id"];
            $sound["uuid"] = $record["sound.uuid"];
            $sound["title"] = $record["sound.title"];
            $sound["uploader"] = $record["sound.uploader"];
            $sound["password"] = $record["sound.password"];
            $sound["file_path"] = $record["sound.file_path"];
            $sound["created_at"] = $record["sound.created_at"];
            while (true) {
                if (!$record) {
                    break;
                }

                if (isset($record["comment.id"])) {
                    array_push($sound["comments"], [
                        "id" => $record["comment.id"],
                        "name" => $record["comment.name"],
                        "content" => $record["comment.content"],
                        "sound_id" => $record["comment.sound_id"],
                        "time" => $record["comment.time"],
                        "created_at" => $record["comment.created_at"],
                    ]);
                }

                $record = $result->fetch_assoc();
            }
        }

        return $sound;
    }

    private static function validate()
    {
        $errors = [];
        return $errors;
    }

    public static function create($uuid, $file_path)
    {
        $title = $_POST["title"];
        $uploader = $_POST["uploader"];
        $hashed_password = password_hash($_POST["password"], PASSWORD_DEFAULT);

        $conn = Connection::get();
        $sql = $conn->prepare("INSERT 
            INTO sounds (`title`, `uploader`, `uuid`, `password`, `file_path`)
            VALUES (?, ?, ?, ?, ?)");
        $sql->bind_param("sssss", $title, $uploader, $uuid, $hashed_password, $file_path);
        $sql->execute();
        return $sql->insert_id;
    }

    public static function delete($instance, $password)
    {
        if (!password_verify($password, $instance["password"])) {
            return false;
        }

        $conn = Connection::get();
        $sql = $conn->prepare("DELETE FROM sounds WHERE id = ?");
        $sql->bind_param("i", $instance["id"]);
        $sql->execute();
        return true;
    }
}
