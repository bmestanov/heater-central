<?php
require_once("./controller.php");
$uploads_dir = $_SERVER['DOCUMENT_ROOT']."/uploads";
$file = $_FILES["upload"];
$tmp_name = $file["tmp_name"];
$name = $file["name"];
$uuid = uniqid();
$file_name = "$uploads_dir/$name";
move_uploaded_file($tmp_name, $file_name);
SoundController::create($uuid, $name);
header("Location: /sounds?id=$uuid");