<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>
        <?=$result["title"]?>
    </title>
    <link rel="stylesheet" href="/assets/css/styles.css">
    <link rel="stylesheet" href="/sounds/show/styles.css">
    <script>
        props = {
            sound: JSON.parse('<?=addslashes(json_encode($result))?>'),
            comments: null,
        };
        props.comments = props.sound.comments;
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <div class="app-nav flex">
        <a class="nav-item" href="/sounds">Upload</a>
        <a id="nav-item-administration" class="nav-item">Administration</a>
    </div>
    <div class="content">
        <div id="administration-container" class="hidden">
            <form class="nav-item" id="delete-form">
                <input id="admin-password-input" class="form-input" type="password" name="password" placeholder="Admin password">
                <input class="form-button" type="button" value="Delete" onClick="deleteSound()">
            </form>
        </div>
        <div id="comments-container" class="flex">
            <div id="empty-comments">
                <h2> No comments yet. Share your sound! </h2>
            </div>
        </div>
        <div id="player-container">
            <audio id="sound" controls controlsList="nodownload">
                <source src="/uploads/<?=$result["file_path"]?>" type="audio/wav">
            </audio>
            <h1 class="title">
                <?=$result["title"]?>
            </h1>
            <p class="subtitle">Uploaded at:
                <?=$result["created_at"]?>
            </p>
            <p class="subtitle">by:
                <?=$result["uploader"] ?: "unknown"?>
            </p>
            <br>
            Add a comment:
            <div class="form">
                <form id="comment-form" action="/comments/create.php" method="post" enctype="multipart/form-data">
                    <input id="title-input" class="form-input" type="text" name="name" placeholder="name" required>
                    <textarea id="content-input" class="form-input comment-input" type="text" name="content"
                        placeholder="content" required></textarea>
                    <input type="hidden" name="sound_id" value="<?=$result["id"]?>">
                    <input id="time-input" type="hidden" name="time" value="0">
                    <input class="form-button" type="submit" value="Go">
                </form>
            </div>
            <div>
            </div>
            <script src="/sounds/show/script.js"></script>
</body>

</html>