const apiUrl = 'http://localhost:6789';
const commentsContainer = document.getElementById('comments-container');
const player = document.getElementById('sound');

function scrobble(time) {
  player.currentTime = time;
  if (player.paused) {
    player.play();
  }
}

function updateCommentTime() {
  const time = player.currentTime;
  document.getElementById('time-input').value = time;
  player.setAttribute('touched', 'true');
}

function addComment(comment) {
  const mm = ('00' + parseInt(comment.time / 60)).slice(-2);
  const ss = ('00' + parseInt(comment.time) % 60).slice(-2);
  const commentDiv = document.createElement('div');
  commentDiv.setAttribute('id', comment.id);
  commentDiv.setAttribute('class', 'comment');
  commentDiv.addEventListener('click', () => scrobble(comment.time));
  commentDiv.innerHTML = `
        <span class="comment-name">${comment.name} at ${mm}:${ss}</span>
        </br>
        <span class="comment-content">${comment.content}</span>
    `;
  document.getElementById('empty-comments').classList.add('hidden');
  commentsContainer.appendChild(commentDiv);
  return commentDiv;
}

function focusComment(commentDiv) {
  if (commentsContainer.hasAttribute('nofocus')) {
    return;
  }
  const activeElements = commentsContainer.getElementsByClassName('comment active');
  for (const el of activeElements) {
    el.classList.remove('active');
  }
  commentDiv.classList.add('active');
  commentDiv.scrollIntoView({
    behavior: 'smooth',
    block: 'center',
  });
}

function postComment(e) {
  e.preventDefault();
  const url = `${apiUrl}/comments/create.php`;
  updateCommentTime();
  fetch(url, {
    method: 'POST',
    body: new FormData(document.getElementById('comment-form')),
  })
    .then(response => response.json())
    .then(comment => {
      props.comments.push(comment);
      const commentDiv = addComment(comment);
      focusComment(commentDiv);
      commentsContainer.setAttribute('nofocus', true);
      setTimeout(() => commentsContainer.removeAttribute('nofocus'), 3000);
    });
    return false;
}

function deleteSound() {
  const input = document.getElementById('admin-password-input');
  const { uuid } = props.sound;
  const password = input.value;
  const url = `${apiUrl}/sounds?id=${uuid}&password=${password}`;
  fetch(url, { method: 'DELETE' })
    .then(response => {
      if (response.ok) {
        window.location = '/sounds';
      } else {
        input.classList.add('error');
      }
    });
}

player.addEventListener('timeupdate', (e) => {
  if (props.comments.length === 0) {
    return;
  }
  const time = e.srcElement.currentTime;
  const { comment } = props.comments.reduce((acc, curr) => {
    const diff = Math.abs(curr.time - time);
    if (diff < acc.diff) {
      return {
        diff,
        comment: curr,
      };
    }
    return acc;
  }, { diff: Infinity });

  const commentDiv = document.getElementById(`${comment.id}`);
  if (!commentDiv.classList.contains('active')) {
    focusComment(commentDiv);
  }
});


if (props.comments.length) {
  document.getElementById('empty-comments').classList.add('hidden');
}

document.getElementById('nav-item-administration').addEventListener('click', function () {
  this.classList.toggle('selected');
  document.getElementById('administration-container').classList.toggle('hidden');
});

document.getElementById('comment-form').addEventListener('submit', postComment);

props.comments.map(addComment);