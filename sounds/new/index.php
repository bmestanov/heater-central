<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Upload a sound</title>
    <link rel="stylesheet" href="../../assets/css/styles.css">
    <script src="js/script.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <div class="center">
        <img class="logo" src="/assets/img/network.svg" alt="logo" />
        <h1>Simple sound sharing</h1>
        <div class="form">
            <form action="create.php" method="post" enctype="multipart/form-data">
                Select a sound to upload:
                <div class="tooltip-container when-hovered">
                    <input class="form-input" type="file" name="upload" accept="audio/mp3" required>
                    <span class="tooltip">Only mp3 files are allowed.</span>
                </div>
                <input class="form-input" type="text" name="title" placeholder="title" required>
                <input class="form-input" type="text" name="uploader" placeholder="uploader">
                <div class="tooltip-container when-focused">
                    <input class="form-input" type="password" name="password" placeholder="password" required>
                    <span class="tooltip">This password will be used for administration, so do not forget it!</span>
                </div>
                <input class="form-button" type="submit" value="Upload" name="submit">
            </form>
        </div>
    </div>
</body>

</html>